const http = require('http');
const { Pool } = require('pg');
const cors = require('cors');
const winston = require('winston');

const hostname = '0.0.0.0';
const port = 3000;

const pool = new Pool({
  user: 'postgres',
  host: 'db-service',
  database: 'mydb',
  password: 'password',
  port: 5432,
});

//const logger = winston.createLogger({
//  transports: [
//    new winston.transports.Console(),
//  ],
//});

const logger = winston.createLogger({
  transports: [
    new winston.transports.File({ filename: '/usr/share/logs-backend/server.log' }), // Запис логів в файл 'server.log'
  ],
});


// Запрос в БД при запуске сервера
pool.query('SELECT NOW()', (err, res) => {
  if (err) {
    logger.error(err); // Логирование ошибки
  } else {
    logger.info(res); // Логирование успешного запроса
  }
});

// Создание сервера
const server = http.createServer((req, res) => {
  res.setHeader('Access-Control-Allow-Origin', '*');
  res.setHeader('Access-Control-Allow-Methods', 'GET');
  res.setHeader('Access-Control-Allow-Headers', 'Content-Type');
  // Обработка запроса на главную страницу
  if (req.url === '/api') {
    // Запрос в БД и отправка ответа клиенту
    pool.query('SELECT NOW()', (err, result) => {
      if (err) {
        logger.error(err);
        res.statusCode = 500;
        res.end('Internal Server Error');
      } else {
        logger.info(result);
        res.statusCode = 200;
        res.setHeader('Content-Type', 'application/json');
        res.end(JSON.stringify(result.rows));
      }
    });
  } else {
    // Обработка запроса на другую страницу
    logger.warn(`Unknown URL: ${req.url}`);
    res.statusCode = 404;
    res.end('Not Found');
  }
});



// Запуск сервера
server.listen(port, hostname, () => {
  logger.info(`Server running at http://${hostname}:${port}/`);
});

